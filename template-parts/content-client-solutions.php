<div class="h2-headings show-on-scroll client-solutions-headings">
    <h2 class="bar-heading first-heading"><?php echo get_field("client_solutions_heading_1"); ?></h2>
    <h2 class="bar-heading second-heading"><?php echo get_field("client_solutions_heading_2"); ?></h2>
    <h2 class="bar-heading third-heading"><?php echo get_field("client_solutions_heading_3"); ?></h2>
    <h1 class="bar-heading fourth-heading"><?php echo get_field("client_solutions_heading_4"); ?></h1>
</div>

<div class="first-text-content show-on-scroll fade-content">
    <?php echo get_field("client_solutions_opening_text"); ?>
</div>

<!-- Start 3 bars -->
<div>
    <h2 class="bar-heading second-content-header show-on-scroll fade-content"><?php echo get_field("client_solutions_second_section_heading"); ?></h2>
<!-- ACF content -->
    <?php
    $bars = get_field('3_bars');
    ?>

    <div class="three-cards">
        <div class="card-outer show-on-scroll first-card">
            <div class="card-inner">
                <h3><?php echo $bars['client_solutions_bar_1_heading']; ?></h3>
                <p><?php echo $bars['client_solutions_bar_1_text']; ?></p>
            </div>
        </div>

        <div class="card-outer show-on-scroll second-card">
            <div class="card-inner">
            <h3><?php echo $bars['client_solutions_bar_2_heading']; ?></h3>
                <p><?php echo $bars['client_solutions_bar_2_text']; ?></p>
            </div>
        </div>

        <div class="card-outer show-on-scroll third-card">
            <div class="card-inner">
            <h3><?php echo $bars['client_solutions_bar_3_heading']; ?></h3>
                <p><?php echo $bars['client_solutions_bar_3_text']; ?></p>
            </div>
        </div>
    </div>
</div> <!-- End 3 bars -->


<!-- Start Carousel -->
<div class="show-on-scroll fade-content">
    <h2 class="bar-heading third-content-header"><?php echo get_field('client_solutions_third_heading') ?></h2>
    <div>

    <div id="carouselExampleControls" class="carousel slide" data-interval="false">
        <div class="carousel-inner">

        <?php if( have_rows('success_stories') ):
                $count = 0; ?>

                <?php while( have_rows('success_stories') ): the_row(); 
                    $header = get_sub_field('header');
                    $lead_text = get_sub_field('lead_text');
                    $large_text_field = get_sub_field('large_text_field');

                    ?>
                    <div  <?php 
                            if (!$count) {
                                ?> class="active carousel-item"<?php 
                            } else {
                                ?> class="carousel-item" <?php

                            }
                            ?>>
                        
                        <div class="carousel-inner-content">
                            <h2 class="carousel-header"><?php echo $header; ?></h2>
                            <div class="carousel-inner-text">
                                <h3><?php echo $lead_text; ?></h3>
                                <?php echo $large_text_field; ?>
                                <!-- <img class="d-block w-100" src="..." alt="First slide"> -->
                            </div>
                        </div>
                    </div>
                <?php $count++;
                    endwhile; ?>
            <?php endif; ?>

            <!-- <div class="carousel-item active">
                <div class="carousel-inner-content">
                    <h2 class="carousel-header">Mishcon De Reya</h2>
                    <div class="carousel-inner-text">
                        <h3>Mishcon De Reya</h3>
                        <p>The world of law is changing. The world of work is changing. The speed of change is accelerating.
                        <br><br>
                            More and more lawyers are rethinking their careers, looking for balance, reassessing their priorities. 
                            More and more lawyers are rethinking their careers, looking for balance, reassessing their priorities. 
                            More and more lawyers are rethinking their careers, looking for balance, reassessing their priorities. 
                            More and more lawyers are rethinking their careers, looking for balance, reassessing their priorities.</p>
                        <img class="d-block w-100" src="..." alt="First slide">
                    </div>
                </div>
            </div> -->

            <!-- <div class="carousel-item">
            <div class="carousel-inner-content">
                    <h2 class="carousel-header">Mishcon De Reya</h2>
                    <div class="carousel-inner-text">
                        <h3>Mishcon De Reya</h3>
                        <p>The world of law is changing. The world of work is changing. The speed of change is accelerating.
                        <br><br>
                            More and more lawyers are rethinking their careers, looking for balance, reassessing their priorities. 
                            More and more lawyers are rethinking their careers, looking for balance, reassessing their priorities. 
                            More and more lawyers are rethinking their careers, looking for balance, reassessing their priorities. 
                            More and more lawyers are rethinking their careers, looking for balance, reassessing their priorities.</p>
                        <img class="d-block w-100" src="..." alt="First slide">
                    </div>
                </div>
            </div> -->

            <!-- <div class="carousel-item">
            <div class="carousel-inner-content">
                    <h2 class="carousel-header">Mishcon De Reya</h2>
                    <div class="carousel-inner-text">
                        <h3>Mishcon De Reya</h3>
                        <p>The world of law is changing. The world of work is changing. The speed of change is accelerating.
                        <br><br>
                            More and more lawyers are rethinking their careers, looking for balance, reassessing their priorities. 
                            More and more lawyers are rethinking their careers, looking for balance, reassessing their priorities. 
                            More and more lawyers are rethinking their careers, looking for balance, reassessing their priorities. 
                            More and more lawyers are rethinking their careers, looking for balance, reassessing their priorities.</p>
                        <img class="d-block w-100" src="..." alt="First slide">
                    </div>
                </div>
            </div>
        </div> -->
        <!-- <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a> -->
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <!-- <img src="<?php echo get_template_directory_uri() ?>/assets/button_blue.png" alt="next button" class="carousel-control-next-icon" aria-hidden="true"> -->
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        </div>
    </div>
</div> <!-- End carousel -->