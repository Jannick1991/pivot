    <?php
    /**
     * Home Page Content
     */
    $id = get_the_id();
    ?>
    <div class="first-section" id="section-1">        
        <div class="scroll-trigger">
        </div>
        <img class="page-title pivot-header bigger" src="<?php echo get_template_directory_uri() ?>/assets/pivot_logo.png');" >
       

        <div class="tagline">
            <?php echo block_tagline($id); ?>
        </div>
        

        <div class="home-card-container scroll-trigger">
            <div class="home-card one">
                <div class="container">
                    <a href="#section-6">
                        <h2 class="home-card-title">Law talent</h2>
                    </a>
                </div>
            </div>

            <div class="home-card two">
                <div class="container">
                    <a href="#section-2">
                        <h2 class="home-card-title">Clients</h2>
                    </a>
                </div>

            </div>

        </div>
        <a href="#section-2" class="arrow">
            <img class="down-arrow down-arrow-white" src="<?php echo get_template_directory_uri() ?>/assets/White-arrow.png');" alt="">
        </a>
    </div>
<div class="home_fixed_elements home_next">
    <a href="#section-3">
        <img class="down-arrow down-arrow-grey" src="<?php echo get_template_directory_uri() ?>/assets/grey-arrow.png');" alt="">
    </a>
</div>
<div class="home_fixed_elements home_decoration">
</div>
<div class="home-section client-section client-section_intro" id="section-2">
    <?php echo block_client_blurb($id); ?>
    <?php echo block_logo_carousel($id); ?>
    <div class="scroll-trigger_below">
        </div>
</div>
<div class="home-section client-section client-section_cta" id="section-3">
    <?php echo block_client_ctas($id); ?>
</div>
<div class="home-section client-section client-section_cases" id="section-4">
    <?php echo block_client_cases($id); ?>
</div>
<div class="home-section client-section client-section_consultants" id="section-5">
    <?php echo block_client_consultants($id); ?>
</div>
<div class="home-section talent-section talent-section_process" id="section-6">
    <?php echo block_talent_intro($id); ?>
    <?php echo block_talent_process($id); ?>
</div>

<div class="home-section talent-section talent-section_current" id="section-7">
    <?php echo block_talent_current($id); ?>
</div>

<div class="home-section team-section" id="section-8">
    <?php echo block_team($id); ?>
</div>

<div class="home-section contact-section" id="contact">
<?php echo do_shortcode( '[contact-form-7 id="29" title="Contact form 1"]' ); ?>
</div>

<!-- <h1 class="page-title"><img src="<?php echo get_template_directory_uri() ?>/assets/PIVOT ASSETS/pivotlogowhite.png" alt="Pivot Logo"></h1> -->
