<?php
$lawyers_headings = get_field('lawyers_4_headings');
?>

<div class="h2-headings lawyers-headings show-on-scroll">
    <h2 class="bar-heading first-heading"><?php echo $lawyers_headings['lawyers_heading_1']; ?></h2>
    <h2 class="bar-heading second-heading"><?php echo $lawyers_headings['lawyers_heading_2']; ?></h2>
    <h2 class="bar-heading third-heading"><?php echo $lawyers_headings['lawyers_heading_3']; ?></h2>
    <h1 class="bar-heading fourth-heading"><?php echo $lawyers_headings['lawyers_heading_4']; ?></h1>
</div>

<div class="first-text-content show-on-scroll fade-content">
    <?php echo get_field('lawyers_entry_text') ?>
</div>

<div class="show-on-scroll fade-content">
    <h2 class="bar-heading">Email Capture</h2>
    <?php echo do_shortcode( '[contact-form-7 id="1234" title="Contact form 1"]' ); ?>
</div>