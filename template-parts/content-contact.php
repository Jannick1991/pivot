<div class="h2-headings show-on-scroll fade-content">
    <h1 class="bar-heading first-heading contact-main-heading"><?php echo get_field('contact_heading_1') ?></h1>
</div>
<div class="first-text-content show-on-scroll fade-content">
    <h3 class="contact-headings second-heading"><?php echo get_field('contact_sub_heading_1') ?></h3>
    <p>Send an email to email@pivot.co.uk or get us on the phone on phone number to obtain a bespoke list of quality candidates that match your requirements.</p>

    <h3 class="contact-headings third-heading"><?php echo get_field('contact_sub_heading_2') ?></h3>
    <p>Want to join the PIVOT family and have the opportunity to work with an amazing portfolio of clients?</p>

<?php
$contact = get_field('contact_information');
?>
    <address class="show-on-scroll fade-content">
        <span>Contact <?php echo $contact['contact_email_name']; ?> via <a href = "mailto: <?php echo $contact['contact_email']; ?>"><?php echo $contact['contact_email']; ?></a></span>
        <span>or <a href="tel:<?php echo $contact['contact_phone_number']; ?>"><?php echo $contact['contact_phone_number']; ?></a> to talk further</span>
    </address>
</div>