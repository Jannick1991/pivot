<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pivot
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site pages" >

<!-- <div class="parallax-ribbon rellax" data-rellax-speed="-4"> -->
<!-- <img src="<?php echo get_template_directory_uri() ?>/assets/PIVOT ASSETS/page_ribbon_repeat_lrg.png" class="page-ribbon rellax" data-rellax-speed="4" alt="Ribbon image"> -->

	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'pivot' ); ?></a>
	<!-- <div class="page-background-grey rellax" data-rellax-speed="-8" style="background-image: url('<?php echo get_template_directory_uri() ?>/assets/PIVOT%20ASSETS/page_ribbon_grey_clean.png');"></div> -->
	<!-- <div class="page-background-grey rellax" data-rellax-speed="-8" style="background-image: url('<?php echo get_template_directory_uri() ?>/assets/PIVOT%20ASSETS/ribbon_long_clear.png');"></div> -->
	<div class="page-background rellax" data-rellax-speed="-8" style="background-image: url('<?php echo get_template_directory_uri() ?>/assets/PIVOT%20ASSETS/page_ribbon_repeat_lrg.png');"></div>
	<!-- <div class="page-background rellax" data-rellax-speed="-8" style="background-image: url('<?php echo get_template_directory_uri() ?>/assets/PIVOT%20ASSETS/ribbon_long_clear.png');"></div> -->

	<!-- <div class="page-background rellax" data-rellax-speed="-8" style="background-image: url('<?php echo get_template_directory_uri() ?>/assets/PIVOT%20ASSETS/ribbon_long_clear.png');"></div> -->

	<!-- <div class="white-strip page"></div> -->
	<div class="grey-strip page"></div>
	<div class="grey-strip-under-white page"></div>



	<header id="masthead" class="site-header">
		<!-- <div class="site-branding">
			<?php
			// the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif;
			$pivot_description = get_bloginfo( 'description', 'display' );
			if ( $pivot_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $pivot_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
			<?php endif; ?>
		</div>.site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<!-- <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'pivot' ); ?></button> -->
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				)
			);
			?>

			<?php
				wp_nav_menu(
					array(
						'theme_location' => 'menu-2',
						'menu_id'        => 'contact-menu',
					)
				);
				?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->


