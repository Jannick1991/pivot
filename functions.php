<?php
/**
 * pivot functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package pivot
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'pivot_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function pivot_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on pivot, use a find and replace
		 * to change 'pivot' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'pivot', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'pivot' ),
				'menu-2' => esc_html__( 'Contact', 'pivot' ),
				'menu-3' => esc_html__( 'Footer', 'pivot' ),

			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'pivot_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'pivot_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function pivot_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'pivot_content_width', 640 );
}
add_action( 'after_setup_theme', 'pivot_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function pivot_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'pivot' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'pivot' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'pivot_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function pivot_scripts() {
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/assets/bootstrap-4.5.3/bootstrap-4.5.3-dist/css/bootstrap.min.css' );
	wp_enqueue_style( 'pivot-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'pivot-style', 'rtl', 'replace' );

	wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.5.1.min.js', array());
	wp_enqueue_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.5.3/umd/popper.min.js', array());
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/assets/bootstrap-4.5.3/bootstrap-4.5.3-dist/js/bootstrap.min.js', array(), true );
	wp_enqueue_script( 'pivot-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	//wp_enqueue_script( 'pivot-parallax', get_template_directory_uri() . '/js/rellax.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'pivot-animations', get_template_directory_uri() . '/js/animations.js', array(), _S_VERSION, true );
	if(is_front_page()){
		wp_enqueue_script( 'flickity', get_template_directory_uri() . '/js/flickity.pkgd.min.js', array(),'2.21', true );
		wp_enqueue_script( 'flickity-init', get_template_directory_uri() . '/js/flickity-init.js', array(), _S_VERSION, true );
		wp_enqueue_style( 'flickity-style', get_template_directory_uri() . '/flickity.css', array(),'2.21' );
		wp_enqueue_script( 'pivot-home-scripts', get_template_directory_uri() . '/js/home-script.js', array(), _S_VERSION, true );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'pivot_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// Google fonts

function wpb_add_google_fonts() {
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css2?family=Montserrat&display=swap', false );
	}
	
	add_action( 'wp_enqueue_scripts', 'wpb_add_google_fonts' );

// Font Awesome
function add_fa() {
	wp_enqueue_script( 'custom-fa', 'https://kit.fontawesome.com/fe58de490b.js"' );
}

add_action( 'wp_enqueue_scripts', 'add_fa' );

/*Use Classic Editor*/

add_filter('use_block_editor_for_post', '__return_false', 10);
/**
 * Carbon Fields.
 */
add_action( 'after_setup_theme', 'crb_load' );
function crb_load() {
    require_once( 'vendor/autoload.php' );
    \Carbon_Fields\Carbon_Fields::boot();
}
require get_template_directory() . '/inc/carbon.php';

/**
 * Functions for displaying blocks
 */
require get_template_directory() . '/inc/blocks.php';


/* Add Google Tag Manager javascript code as close to 
the opening <head> tag as possible
=====================================================*/
function add_gtm_head(){
?>
 
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W4MZB8C');</script>
<!-- End Google Tag Manager -->
 
<?php 
}
add_action( 'wp_head', 'add_gtm_head', 10 );
 
/* Add Google Tag Manager noscript codeimmediately after 
the opening <body> tag
========================================================*/
function add_gtm_body(){
?>
 
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W4MZB8C"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
 
<?php 
}
add_action( 'wp_body_open', 'add_gtm_body' );