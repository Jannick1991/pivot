/**
 * Initializes FLickity Carousel on Home Page
 */
var elem = document.querySelectorAll('.pivot_carousel');
elem.forEach((element)=>{
  const options = {
    wrapAround: true,
    autoPlay: false,
    pageDots: false,
    freeScroll: false,
    imagesLoaded: true,
    selectedAttraction: 0.01,
    friction: 0.2,
    lazyLoad:false,
  }
  if(element.classList.contains('pivot_carousel--logo')){
    options['imagesLoaded'] = false;
    options['autoPlay'] = 2000;
    options['prevNextButtons'] = false;
  }


  var flkty = new Flickity( element, options);
  const forwardButtons = document.querySelectorAll('.pivot_item--forward');
  if (forwardButtons){
    forwardButtons.forEach(button => button.addEventListener('click',()=>{
      flkty.next(true,false);
    }))
  }
  }
)



