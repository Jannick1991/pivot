/*Scripts controlling Home Page Sections*/
const nextAnchor = document.querySelector('.home_next');
const decoration = document.querySelector('.home_decoration');
const site = document.querySelector('#page');
const firstSection = document.querySelector('.first-section');
const wayFinder = document.querySelector('.home-wayfinder');
if(window.innerWidth<600){
 var threshold = {threshold: 0.1};
}
else{
  var threshold = {threshold: 0.1};
}
if(!!window.IntersectionObserver){
    
    let observer = new IntersectionObserver((entries, observer) => { 
      entries.forEach(entry => {
      if(entry.isIntersecting){
        if(entry.target.classList.contains('home-section') && !entry.target.classList.contains('contact-section')){
          nextAnchor.firstElementChild.href=`#${entry.target.nextElementSibling.id}`;
        }
        nextAnchor.classList.remove('hide');
        if(site.classList.contains('hide_fixed')){
            site.classList.remove('hide_fixed');
        }
        entry.target.classList.add('reveal');
        if(entry.target.id=='section-2'){
          firstSection.classList.add('expand');
          firstSection.classList.remove('shrink');
        }
        if(entry.target.classList.contains('client-section')){
          wayFinder.innerHTML = 'Clients'
        }
        if(entry.target.classList.contains('talent-section')){
          wayFinder.innerHTML = 'Law Talent'
        }
        if(entry.target.classList.contains('team-section')){
          wayFinder.innerHTML = 'Meet The Team'
        }
        if(entry.target.classList.contains('contact-section')){
          wayFinder.innerHTML = 'Register';
          nextAnchor.classList.add('hide');
        }
      }
      });
    }, threshold);
    document.querySelectorAll('.home-section').forEach(section => { observer.observe(section) });
    observer.observe(document.querySelector('.scroll-trigger_below'))
    let topObserver = new IntersectionObserver((entries,observer) =>{
      entries.forEach(entry => {
        if(entry.isIntersecting){
          if(firstSection.classList.contains('expand')){
            firstSection.classList.remove('expand');
            firstSection.classList.add('shrink');
          }
          site.classList.add('hide_fixed');
          wayFinder.innerHTML = '';
        }
      });
    },{rootMargin:"0px 0px 0px 0px"})
    document.querySelectorAll('.scroll-trigger').forEach(trigger => {topObserver.observe(trigger)});
}
  
else {
    site.classList.add('no_intersection');
    site.classList.remove('hide_fixed');
    document.querySelectorAll('.home-section').forEach(section => { section.classList.add('reveal') });
}
  

document.querySelector("#file").onchange = function(){
  document.querySelector("#CV_button").value = `Uploaded: ${this.files[0].name}`;
}


/*Email Validation for WPCF7*/
const emailOriginal = document.querySelector('#email');
const emailConfirm = document.querySelector('#email-confirm');
if(emailConfirm && emailOriginal){
  emailConfirm.addEventListener('blur',compareEmails);
  emailOriginal.addEventListener('blur',compareEmails);
}

function compareEmails(e){
  if(emailOriginal.value.length==0 || emailConfirm.value.length==0){
    return;
  }
  if(emailOriginal.value!==emailConfirm.value){
    document.querySelector('.wpcf7-submit').disabled=true;
    let errMsgSpan = document.createElement('span');
    errMsgSpan.innerText = 'Your email addresses don\'t match';
    errMsgSpan.classList.add('wpcf7-not-valid-tip','email-confirm-error');
    emailOriginal.insertAdjacentElement("afterend",errMsgSpan);
  }
  else{
    document.querySelector('.wpcf7-submit').disabled=false;
    let errMsgs = document.querySelectorAll('.email-confirm-error');
    if(errMsgs){
      errMsgs.forEach(msg=>{msg.remove()});
    }

  }
}

