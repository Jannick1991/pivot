<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pivot
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<!-- old <div id="page" class="site home-background" style="background-image: url('<?php echo get_template_directory_uri() ?>/assets/PIVOT%20ASSETS/home_ribbon_bottom_new_changes.png');"> -->
<div id="page" class="site home-background hide_fixed">

<!-- <img src="<?php echo get_template_directory_uri() ?>/assets/PIVOT ASSETS/home_ribbon_repeat.png" class="home-ribbon" alt="Ribbon image"> -->

<!-- <div class="home-background-ribbon" style="background-image: url('<?php echo get_template_directory_uri() ?>/assets/PIVOT%20ASSETS/home_ribbon_repeat.png');" > </div> -->

	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'pivot' ); ?></a>
	<!-- <div class="home-full-background" style="background-image: url('<?php echo get_template_directory_uri() ?>/assets/PIVOT%20ASSETS/page_ribbon_grey_clean.png');"></div> -->
	<!-- <div class="home-full-background" style="background-image: url('<?php echo get_template_directory_uri() ?>/assets/PIVOT%20ASSETS/page_ribbon_grey_clean.png');"></div> -->

	<header id="masthead" class="site-header home-page">
		<!-- <div class="site-branding">
			<?php
			// the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif;
			$pivot_description = get_bloginfo( 'description', 'display' );
			if ( $pivot_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $pivot_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
			<?php endif; ?>
		</div>.site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<!-- <button class="menu-toggle fa" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'pivot' ); ?></button> -->
			<div class="hamburger menu-toggle fa">
				<div class="bar1"></div>
				<div class="bar2"></div>
				<div class="bar3"></div>
			</div>
			
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				)
			);
			?>

			<!-- <?php
				wp_nav_menu(
					array(
						'theme_location' => 'menu-2',
						'menu_id'        => 'contact-menu',
					)
				);
				?> -->
				<div class="home-wayfinder"></div>
		</nav><!-- #site-navigation -->



	</header><!-- #masthead -->
