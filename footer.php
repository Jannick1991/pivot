<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pivot
 */

?>

	<footer class="footer" id="colophon" class="site-footer">
	<a href="https://facebook.com"><i class="fab fa-facebook"></i></a>
	<a href="https://instagram.com"><i class="fab fa-instagram"></i></a>
	<a href="https://linkedin.com"><i class="fab fa-linkedin-in"></i></a>
	</footer><!-- #colophon -->
</div><!-- #page -->


<?php wp_footer(); ?>
<!-- </div> end paralax-ribbon -->
</body>
</html>
