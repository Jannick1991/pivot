<?php
/**
 * Registering Carbon Fields
 */

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'pivot_post_meta' );
function pivot_post_meta() {
    Container::make( 'post_meta', 'Landing Section' )
    ->where( 'post_type', '=', 'page' )
    ->where( 'post_id', '=', get_option( 'page_on_front' ))
    ->set_context( 'carbon_fields_after_title' )
    ->add_fields( array(
                Field::make('text','pivot_home_blurb','Slogan'),
            )
    );
    Container::make( 'post_meta', 'Client Section' )
    ->where( 'post_type', '=', 'page' )
    ->where( 'post_id', '=', get_option( 'page_on_front' ))
    ->set_context( 'carbon_fields_after_title' )
    ->add_fields( array(
                Field::make('text','pivot_client_title','Title'),
                Field::make('rich_text','pivot_client_description','Description'),
                Field::make( 'complex', 'pivot_client_logos' ,'Client Logo Carousel')
                ->add_fields(array(
                    Field::make('image','pivot_client_logo','Add Logos')->set_width(10),
                ))->set_layout('tabbed-horizontal'),
                Field::make( 'complex', 'pivot_client_cta' ,'Call to Action Cards')
                ->add_fields(array(
                    Field::make('text','pivot_cta_title', 'Title')->set_width(50),
                    Field::make('rich_text','pivot_cta_desc','Description')->set_width(50),
                ))
                ->set_max( 3 )
                ->set_help_text('Maximum three cards')
                ->set_layout('tabbed-horizontal'),
                Field::make( 'complex', 'pivot_client_cases' ,'Case Study Cards')
                ->add_fields(array(
                    Field::make('image','pivot_case_logo','Logo')->set_width(10),
                    Field::make('rich_text','pivot_case_brief','Brief')->set_width(35),
                    Field::make('rich_text','pivot_case_solution','Solution')->set_width(35),
                ))->set_layout('tabbed-horizontal'),
                Field::make( 'complex', 'pivot_client_consultants' ,'Consultants Cards')
                ->add_fields(array(
                    Field::make('image','pivot_consult_portrait','Portrait')->set_width(10),
                    Field::make('text','pivot_consult_name','Name')->set_width(35),
                    Field::make('rich_text','pivot_consult_bio','Biography')->set_width(35),
                ))->set_layout('tabbed-horizontal'),
            )
    );
    Container::make( 'post_meta', 'Law Talent Section' )
    ->where( 'post_type', '=', 'page' )
    ->where( 'post_id', '=', get_option( 'page_on_front' ))
    ->set_context( 'carbon_fields_after_title' )
    ->add_fields( array(
                Field::make('text','pivot_talent_title','Title'),
                Field::make('text','pivot_talent_subtitle','Subtitle'),
                Field::make('rich_text','pivot_talent_text','Text'),
                Field::make( 'complex', 'pivot_talent_process' ,'Talent Process Cards')
                ->add_fields(array(
                    Field::make('text','pivot_process_title','Title')->set_width(50),
                    Field::make('rich_text','pivot_process_desc','Description')->set_width(50),
                ))->set_layout('tabbed-horizontal'),
                Field::make( 'complex', 'pivot_talent_current' ,'Talent Current Assignment Cards')
                ->add_fields(array(
                    Field::make('text','pivot_current_title','Title')->set_width(50),
                    Field::make('rich_text','pivot_current_desc','Description')->set_width(50),
                ))->set_layout('tabbed-horizontal'),
            )
    );
    Container::make( 'post_meta', 'Meet The Team Section' )
    ->where( 'post_type', '=', 'page' )
    ->where( 'post_id', '=', get_option( 'page_on_front' ))
    ->set_context( 'carbon_fields_after_title' )
    ->add_fields( array(
                Field::make( 'complex', 'pivot_team' ,'Meet The Team Cards')
                ->add_fields(array(
                    Field::make('text','pivot_team_name','Name')->set_width(50),
                    Field::make('rich_text','pivot_team_bio','Biography')->set_width(50),
                )),
            )
    );
}
