<?php
/**
 * Displaying blocks
 */

 function block_tagline($id){
    $tagline = carbon_get_post_meta( $id,  'pivot_home_blurb' );
    $output='<h2>' . $tagline . '</h2>';
    return $output;
 }

 function block_client_blurb($id){
     $title = carbon_get_post_meta( $id, 'pivot_client_title');
     $description = apply_filters('the_content',carbon_get_post_meta($id, 'pivot_client_description'));
     $output = '<div class="container container_intro container_client--intro">
                    <h2 class="intro--header">Pivot</h2>
                    <h2 class="intro--subheader">' . $title . '</h2>' . 
                    $description . 
                    '</div>';
    return $output;
 }

 function block_logo_carousel($id){
    $logos = carbon_get_post_meta( $id, 'pivot_client_logos');
    $output = '<div class="pivot_carousel pivot_carousel--logo">';
    foreach($logos as $logo){
        $image = wp_get_attachment_image($logo['pivot_client_logo'],'medium');
        $carousel.='<div class="pivot_item">' . $image . '</div>';
    }
    $output.=$carousel.$carousel.$carousel;
    $output.='</div>';
    
    return $output;
 }

 function block_client_ctas($id){
    $cards=carbon_get_post_meta( $id, 'pivot_client_cta');
    $output = '<div class="container container_cards container_client--cards">';
    foreach($cards as $card){
        $text = apply_filters('the_content',$card['pivot_cta_desc']);
        $output.='<div class="card card_client card_client--cta"><h3>' .
        $card['pivot_cta_title'] . 
        '</h3>' . 
        $text . 
        '<a href="#contact">Contact Us</a>
        </div>';
    }
    $output.='</div>';
    return $output;
}

function block_client_cases($id){
    $cards=carbon_get_post_meta( $id, 'pivot_client_cases');
    $output = '<div class="container container_carousel container_client--cases">
                    <h2>Case Studies</h2>
                    <div class="pivot_carousel pivot_carousel--cases">';
    foreach($cards as $card){
        $brief = strlen($card['pivot_case_brief'])>=200?substr($card['pivot_case_brief'],0,200)."..." : $card['pivot_case_brief'];
        $solution = strlen($card['pivot_case_solution'])>=200?substr($card['pivot_case_solution'],0,200)."..." : $card['pivot_case_solution'];
        $logo= wp_get_attachment_image($card['pivot_case_logo'],'medium');
        $output.='<div class="pivot_item"><div class="pivot_item--top">' .
        $logo . 
        '<div class="pivot_item--forward"></div></div><div class="pivot_item--bottom">' . 
        '<p><strong>BRIEF </strong>' .$brief . '</p>'.
        '<p><strong>SOLUTION </strong>' .$solution . '</p></div></div>';
    }
    $output.='</div></div>';
    return $output;
}

function block_client_consultants($id){
    $cards=carbon_get_post_meta( $id, 'pivot_client_consultants');
    $output = '<div class="container container_cards container_client--cards">
    <h2>Meet Some of Our Consultants</h2>';
    foreach($cards as $card){
        $text = apply_filters('the_content',$card['pivot_consult_bio']);
        $portrait = wp_get_attachment_image($card['pivot_consult_portrait'],'medium');
        $output.='<div class="card card_client card_client--consult">'. 
        $portrait .
        '<div class="card-right"><h3>' .
        $card['pivot_consult_name'] . 
        '</h3>' . 
        $text . 
        '</div></div>';
    }
    $output.='</div>';
    return $output;
}

function block_talent_intro($id){
    $title = carbon_get_post_meta( $id, 'pivot_talent_title');
    $subtitle = carbon_get_post_meta( $id, 'pivot_talent_subtitle');
    $description = apply_filters('the_content',carbon_get_post_meta($id, 'pivot_talent_text'));
    $output = '<div class="container container_intro container_talent--intro">
                   <h2 class="intro--header">' .$title . '</h2>
                   <h2 class="intro--subheader">' . $subtitle . '</h2>' . 
                   $description . 
                   '</div>';
   return $output;
}

function block_talent_process($id){
    $cards=carbon_get_post_meta( $id, 'pivot_talent_process');
    $output = '<div class="pivot_carousel pivot_carousel--process">';
    $count = 1;
    foreach($cards as $card){
        $text = apply_filters('the_content',$card['pivot_process_desc']);
        $text = strlen($text)>=512?substr($text,0,512)."..." : $text;

        $title = $card['pivot_process_title'];
        $output.='<div class="pivot_item"><div class="pivot_item--top"><h3>' .
        $title . 
        '</h3><div class="number">' . $count . '<div class="pivot_item--forward"></div></div>
        </div><div class="pivot_item--bottom">' . 
        $text . 
        '<p><a href="/#contact">Contact Us</a></p></div></div>';
        $count++;
    }
    $output.='</div>';
    return $output;

}

function block_talent_current($id){
    $cards=carbon_get_post_meta( $id, 'pivot_talent_current');
    $output = '<div class="container container_cards container_talent--cards">
    <h2>Current Assignments</h2>';
    foreach($cards as $card){
        $text = apply_filters('the_content',$card['pivot_current_desc']);
        $output.='<div class="card card_talent card_talent--current"><h3>' .
        $card['pivot_current_title'] . 
        '</h3>' . 
        $text . 
        '</div>';
    }
    $output.='</div>';
    return $output;
}

function block_team($id){
    $cards=carbon_get_post_meta( $id, 'pivot_team');
    $output = '<div class="container container_cards container_team--cards">
                <h2 class="intro--header">Meet The Team</h2>';
    foreach($cards as $card){
        $text = apply_filters('the_content',$card['pivot_team_bio']);
        $output.='<div class="card card_team card_team--team"><h3>' .
        $card['pivot_team_name'] . 
        '</h3>' . 
        $text . 
        '</div>';
    }
    $output.='</div>';
    return $output;
}